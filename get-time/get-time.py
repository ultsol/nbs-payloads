#!/usr/bin/env python2

from time import gmtime, strftime
import os

print 'Environment:', os.environ
print 'Time:', strftime("%Y-%m-%d %H:%M:%S", gmtime())

